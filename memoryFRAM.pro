HEADERS   = dialog.h \
            config.h \
            mainwindow.h
RESOURCES = memoryFRAM.qrc
SOURCES   = dialog.cpp \
            config.cpp \
            main.cpp \
            mainwindow.cpp

QT += sql widgets
QT += xml widgets
requires(qtConfig(tableview))

# install
target.path = $$/memoryFRAM
INSTALLS += target

FORMS +=
