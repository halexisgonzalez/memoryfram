#include "config.h"

Config::Config(QWidget *parent) : QDialog(parent)
{
    QGroupBox *inputWidgetBox = createInputWidgets();
    QDialogButtonBox *buttonBox = createButtons();

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(inputWidgetBox);
    layout->addWidget(buttonBox);
    setLayout(layout);

    setWindowTitle(tr("Configuración de Memoria"));
}

void Config::submit()
{
    QString last = lastEditor->text();
    QString data = dataEditor->text();

    if (last.isEmpty() || data.isEmpty()) {
        QString message(tr("Por favor complete todos los datos."));
        QMessageBox::information(this, tr("Falta completar campos"), message);
    }
    else {
        lastPos = lastEditor->text();
        tamPos = tamMem->text();
        dataPos = dataEditor->text();

        accept();
    }
}

QGroupBox *Config::createInputWidgets()
{
    QGroupBox *box = new QGroupBox(tr("Genera un nuevo .JSON"));

    QLabel *beginLabel = new QLabel(tr("Desde"));
    QLabel *lastLabel = new QLabel(tr("Hasta"));
    QLabel *dispLabel = new QLabel(tr("Tamaño"));
    QLabel *dataLabel = new QLabel(tr("Dato"));

    QLabel *initialLabel = new QLabel(tr("0x0000"));

    lastEditor = new QLineEdit;
    lastEditor->setPlaceholderText("Ej. 0x7FFF");

    tamMem = new QLabel(tr("8192"));

    dataEditor = new QLineEdit;
    dataEditor->setPlaceholderText("Ej. FREE");

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(beginLabel, 0, 0);
    layout->addWidget(initialLabel, 0, 1);
    layout->addWidget(lastLabel, 1, 0);
    layout->addWidget(lastEditor, 1, 1);
    layout->addWidget(dispLabel, 2, 0);
    layout->addWidget(tamMem, 2, 1);
    layout->addWidget(dataLabel, 3, 0);
    layout->addWidget(dataEditor, 3, 1);
    box->setLayout(layout);

    return box;
}

QDialogButtonBox *Config::createButtons()
{
    QPushButton *closeButton = new QPushButton(tr("&Cancelar"));
    QPushButton *submitButton = new QPushButton(tr("&Aceptar"));

    closeButton->setDefault(true);

    connect(closeButton, &QPushButton::clicked, this, &Config::close);
    connect(submitButton, &QPushButton::clicked, this, &Config::submit);

    QDialogButtonBox *buttonBox = new QDialogButtonBox;
    buttonBox->addButton(submitButton, QDialogButtonBox::ResetRole);
    buttonBox->addButton(closeButton, QDialogButtonBox::RejectRole);

    return buttonBox;
}
