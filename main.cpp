#include "mainwindow.h"

#include <QApplication>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(memoryFRAM);

    QApplication app(argc, argv);
    MainWindow window;

    window.setWindowIcon(QIcon(":/images/logo.png"));
    window.show();
    return app.exec();
}
