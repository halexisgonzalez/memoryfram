# Mapa de Memoria
<center><img src="https://gitlab.com/halexisgonzalez/memoryfram/-/raw/main/GUI.png"></center>

El mapeo y gestión de memoria< se refiere a la asignación y control del espacio de memoria disponible para un programa en un sistema computacional.
Estos procesos son críticos para garantizar que los programas tengan acceso al espacio de memoria necesario y que los recursos de memoria del sistema se utilicen de manera eficiente.

El objetivo de este programa es administrar de manera eficiente y ordenada la memoria de comunicación entre la plataforma de control y la interfaz de usuario
(GUI) del equipo LUFT5 de Leistung+. Además, permite la posibilidad de crear confirmaciones del uso de la memoria en un almacenamiento remoto, lo que garantiza un acceso actualizado y compartido de la información para todo el equipo de trabajo."

### Pre-requisitos 📋

Sistemas Operativos Compatibles:
- Windows 10
- Windows 11
- Ubuntu 20.04
- Ubuntu 22.04

### Instalación 🔧

- [1] Clonar repositorio
- [2] Cambiar de rama a _"TAG <versión>"_
- [3] Ejecutar el instalador
 
## Construido con 🛠️

* [Qt Creator 7.0.2](https://www.qt.io/blog/qt-creator-7.0.2-released) - IDE para desarrollar aplicaciones con Qt. Proporciona herramientas de programación y depuración para crear aplicaciones de GUI de manera eficiente.
* [Qt 6.2.4](https://www.qt.io/blog/qt-6.2.4-released) - Qt 6.2.4 es una versión estable y actualizada del framework de desarrollo de software Qt. Proporciona correcciones de errores, mejoras en la compatibilidad con sistemas operativos (Win11) y nuevas características de biblioteca, como el soporte para Vulkan 1.2. Es recomendado para los usuarios de Qt 6 actualizar a esta versión.
* [JSON](https://www.json.org/json-es.html) - JSON puede utilizarse como una forma simple y ligera de almacenar y acceder a datos en una aplicación, aunque no está diseñado específicamente como un sistema de gestión de bases de datos. Puede ser una opción adecuada para aplicaciones más pequeñas o para desarrolladores que no desean configurar y administrar un sistema de gestión de bases de datos completo.

## Despliegue y Desarrollo 📦

- [1] Clonar repositorio
- [2] Cambiar de rama a _"develop"_
- [3] Ejecutar QtCreator
- [4] Crear kit para desktop

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/halexisgonzalez/memoryfram/-/wikis/Memory-Map)

## Versionado 📌

Usamos [SemVer](https://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.com/halexisgonzalez/memoryfram/-/tags).

## Autores ✒️
* **Ing. González Alexis** - *Desarrollo y Documentación* - [halexisgonzalez](@halexisgonzalez)

---