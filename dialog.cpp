#include "dialog.h"

#define STYLESHEET_FILE ":/resources/cssFormat.qss"

Dialog::Dialog(QWidget *parent) : QDialog(parent)
{
    QGroupBox *inputWidgetBox = createInputWidgets();
    QDialogButtonBox *buttonBox = createButtons();

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(inputWidgetBox);
    layout->addWidget(buttonBox);
    setLayout(layout);

    setWindowTitle(tr("Agregar Posición de Memoria"));

    // StyleSheet
    QFile styleSheetFile(STYLESHEET_FILE);
    if (!styleSheetFile.open(QFile::ReadOnly))
    {
       qWarning("Unable to open StyleSheet %s", STYLESHEET_FILE);
    }
    QTextStream out(&styleSheetFile);
    this->setStyleSheet(out.readAll());
}

void Dialog::submit()
{
    QString address = beginEditor->text();    
    QString descrip = descripEditor->text();
    QString size = sizeEditor->text();
    QString group = groupEditor->text();

    if (address.isEmpty() || descrip.isEmpty() || size.isEmpty() || group.isEmpty()) {
        QString message(tr("Por favor complete todos los datos."));
        QMessageBox::information(this, tr("Falta completar campos"), message);
    }
    else {
        if(address.length() > 6){
            QString message(tr("La posición debe tener 6 digitos máximo: Ej. 0x1234"));
            QMessageBox::information(this, tr("Posición incorrecta"), message);
        }
        else{
            bool ok;
            int addressint = address.toInt(&ok, 16);
            int sizeint = size.toInt(&ok, 16);
            if(addressint > 0x7FFC){
                QString message(tr("OVERFLOW. La memoria posee un rango de: 0x0000 - 0x7FFF.\n"
                                   "Recordar: Última posición de memoria coherente: 0x7FFC."));
                QMessageBox::warning(this, tr("OVERFLOW en la memoria"), message);
            }
            else{
                QString formatOk = checkFormat(address);
                QString sizeOk = checkFormat(size);
                if((formatOk == "Error") || (sizeOk == "Error")){
                    QString message(tr("ERROR. Se debe utilizar valores en formato hexadecimal\n"
                                       "para los campos 'Desde' y 'Peso'."));
                    QMessageBox::warning(this, tr("ERROR en el formato"), message);
                }
                else{
                    if((addressint % 4) || (sizeint % 4)){
                        QString message(tr("ERROR. Los valores de la posición 'Desde' y del 'Peso'\n"
                                           "deben ser múltiplos de 4 (hexadecimal).\n\n"
                                           "Ej. 0x4, 0x8, 0xC, 0x10, 0x14, 0x18, 0x1C, etc."));
                        QMessageBox::warning(this, tr("ERROR en el formato"), message);
                    }
                    else{
                        beginPos = formatOk;
                        sizePos = sizeEditor->text();
                        descripPos = descripEditor->text();
                        groupPos = groupEditor->text();
                        obsPos = obsEditor->text();

                        accept();
                    }
                }
            }
        }
    }
}

QString Dialog::checkFormat(const QString &str)
{
   // Eliminar los espacios en blanco del principio y final de la cadena
   QString strSlice = str.trimmed();

   // Check if string starts with "0x"
   if (strSlice.startsWith("0x")) {
       // Extraer los caracteres hexadecimales de la cadena
       strSlice = strSlice.mid(2);
   }

   // Completar con ceros a la izquierda si la cadena es menor a 4 caracteres
   if (strSlice.length() < 4) {
       strSlice = QString(4 - strSlice.length(), '0') + strSlice;
   }

   // Convertir la cadena en un valor hexadecimal y devolverlo como cadena
   bool ok;
   uint value = strSlice.toUInt(&ok, 16);


   if (ok) {
       return "0x" + QString::number(value, 16).rightJustified(4, '0').toUpper();
   } else {
       return "Error";
   }
}

void Dialog::revert()
{
    beginEditor->clear();
    descripEditor->clear();
    groupEditor->clear();
    sizeEditor->clear();
    obsEditor->clear();
}

QGroupBox *Dialog::createInputWidgets()
{
    QGroupBox *box = new QGroupBox(tr("Agregar Posición"));

    QLabel *beginLabel = new QLabel(tr("Desde [hex]"));
    QLabel *sizeLabel = new QLabel(tr("Peso [hex]"));
    QLabel *descripLabel = new QLabel(tr("Descripción"));
    QLabel *groupLabel = new QLabel(tr("Grupo"));
    QLabel *obsLabel = new QLabel(tr("Observaciones:"));

    beginEditor = new QLineEdit;
    beginEditor->setPlaceholderText("Ej. 0x2020");

    sizeEditor = new QLineEdit;
    sizeEditor->setPlaceholderText("Ej. 4");

    descripEditor = new QLineEdit;
    descripEditor->setPlaceholderText("Ej. Parámetro Nuevo");

    groupEditor = new QLineEdit;
    groupEditor->setPlaceholderText("Ej. Grupo Nuevo");

    obsEditor = new QLineEdit;
    obsEditor->setPlaceholderText("Opcional");

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(beginLabel, 0, 0);
    layout->addWidget(beginEditor, 0, 1);
    layout->addWidget(sizeLabel, 1, 0);
    layout->addWidget(sizeEditor, 1, 1);
    layout->addWidget(descripLabel, 2, 0);
    layout->addWidget(descripEditor, 2, 1);
    layout->addWidget(groupLabel, 3, 0);
    layout->addWidget(groupEditor, 3, 1);
    layout->addWidget(obsLabel, 4, 0, 1, 2);
    layout->addWidget(obsEditor, 5, 0, 1, 2);
    box->setLayout(layout);

    return box;
}

QDialogButtonBox *Dialog::createButtons()
{
    QPushButton *closeButton = new QPushButton;
    closeButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    closeButton->setIcon(QIcon(":/images/closeIcon.png"));
    closeButton->setIconSize(QSize(20, 20));

    QPushButton *cleanButton = new QPushButton;
    cleanButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    cleanButton->setIcon(QIcon(":/images/cleanIcon.png"));
    cleanButton->setIconSize(QSize(20, 20));

    QPushButton *submitButton = new QPushButton;
    submitButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    submitButton->setIcon(QIcon(":/images/doneIcon.png"));
    submitButton->setIconSize(QSize(20, 20));

    closeButton->setDefault(true);

    connect(closeButton, &QPushButton::clicked, this, &Dialog::close);
    connect(cleanButton, &QPushButton::clicked,  this, &Dialog::revert);
    connect(submitButton, &QPushButton::clicked, this, &Dialog::submit);

    QDialogButtonBox *buttonBox = new QDialogButtonBox;
    buttonBox->addButton(submitButton, QDialogButtonBox::ResetRole);
    buttonBox->addButton(cleanButton, QDialogButtonBox::ResetRole);
    buttonBox->addButton(closeButton, QDialogButtonBox::RejectRole);

    return buttonBox;
}
