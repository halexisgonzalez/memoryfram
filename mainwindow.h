#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDomDocument>
#include <QMainWindow>
#include <QModelIndex>
#include <QProgressBar>
#include <QGridLayout>
#include <QPushButton>

QT_BEGIN_NAMESPACE
class QComboBox;
class QFile;
class QGroupBox;
class QLabel;
class QListWidget;
class QSqlRelationalTableModel;
class QTableView;
class QTableWidget;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

private slots:
    void about();
    void addPosit();
    void deletePosit();
    void showPositDetails();
    void openFile();
    void saveFile();
    void initialProgram();
    void filter(const QString&);
    QString addedAddresses(QString, QString);
    void updateProgressBar();
    void configMemory();
    void lastModifiedFile(QString);
    void showBitsDetails(bool);

    void onTableWidgetCellClicked(int row, int column);
    void editButtonSlot();
    void fullButtonSlot();
    void emptyButtonSlot();
    void revertButtonSlot();
    void groupButtonSlot();
    void filterButtonSlot();

private:
    void adjustHeader();
    void createMenuBar();
    void hideLabels();

    bool groupButtonPressed;
    bool filterButtonPressed;

    int totalNoFree;
    int bitSelectedRow;
    int bitSelectedColumn;

    QString lastModified;
    QString itemMain;
    QString prevValueBit;

    QGridLayout *layoutMain;

    QGroupBox *createBrowserGroupBox();
    QGroupBox *createDescriptionGroupBox();
    QGroupBox *createDetailsGroupBox();
    QGroupBox *createBitsGroupBox();
    QGroupBox *createStatusGroupBox();
    QGroupBox *createControlGroupBox();
    QGroupBox *createStsBitsGroupBox();

    QLabel *infoLabel;
    QLabel *descripLabel;
    QLabel *obsLabel;
    QLabel *relationLabel;
    QLabel *beforeData;

    QTableWidget *memoryTable;
    QTableWidget *bitsTable;

    QAction *openAction;
    QAction *addAction;
    QAction *deleteAction;
    QAction *saveAction;
    QAction *configAction;
    QAction *quitAction;
    QAction *aboutAction;

    QProgressBar *progressBar;

    QPushButton *editButton;
    QPushButton *fullButton;
    QPushButton *emptyButton;
    QPushButton *revertButton;
    QPushButton *groupButton;
    QPushButton *filterButton;
    QPushButton *addButton;
    QPushButton *deleteButton;
};

#endif
