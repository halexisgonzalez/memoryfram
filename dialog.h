#ifndef DIALOG_H
#define DIALOG_H

#include <QtWidgets>
#include <QtSql>
#include <QtXml>

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);

    QString beginPos;
    QString sizePos;
    QString descripPos;
    QString groupPos;
    QString obsPos;

private slots:
    void revert();
    void submit();

    QString checkFormat(const QString& str);

private:
    QDialogButtonBox *createButtons();
    QGroupBox *createInputWidgets();

    QLineEdit *beginEditor;
    QLineEdit *sizeEditor;
    QLineEdit *descripEditor;
    QLineEdit *groupEditor;
    QLineEdit *obsEditor;
};

#endif
