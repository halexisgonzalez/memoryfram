#ifndef CONFIG_H
#define CONFIG_H

#include <QtWidgets>
#include <QtSql>
#include <QtXml>

class Config : public QDialog
{
    Q_OBJECT

public:
    Config(QWidget *parent = nullptr);

    QString lastPos;
    QString tamPos;
    QString dataPos;

private slots:
    void submit();

private:
    QDialogButtonBox *createButtons();
    QGroupBox *createInputWidgets();

    QLabel *tamMem;

    QLineEdit *lastEditor;
    QLineEdit *descripEditor;
    QLineEdit *dataEditor;

};

#endif
