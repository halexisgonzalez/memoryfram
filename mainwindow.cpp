#include "mainwindow.h"
#include "dialog.h"
#include "config.h"

#include <QtWidgets>
#include <QtSql>
#include <QtXml>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#define STYLESHEET_FILE ":/resources/cssFormat.qss"

QMap<int, QJsonObject> memoryMap;
QHash<QString, int> indexAdds;

MainWindow::MainWindow()
{
    createMenuBar();
    resize(800, 600);
    setWindowTitle(tr("Mapa de memoria FRAM"));

    // StyleSheet
    QFile styleSheetFile(STYLESHEET_FILE);
    if (!styleSheetFile.open(QFile::ReadOnly))
    {
       qWarning("Unable to open StyleSheet %s", STYLESHEET_FILE);
    }
    QTextStream out(&styleSheetFile);
    this->setStyleSheet(out.readAll());

    QLabel *iconLogoLabel = new QLabel;
    iconLogoLabel->setPixmap(QPixmap(":/images/logoLeistung.png").scaled(500, 70, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    QPushButton *openButton = new QPushButton;
    openButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    openButton->setIcon(QIcon(":/images/openIcon.png"));
    openButton->setIconSize(QSize(200, 200));
    QObject::connect(openButton, &QPushButton::clicked, this, &MainWindow::openFile);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(iconLogoLabel, 0, {Qt::AlignHCenter});
    layout->addWidget(openButton, 0, {Qt::AlignCenter});

    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    setCentralWidget(widget);
}

void MainWindow::initialProgram()
{
    totalNoFree = 0;

    QGroupBox *positionMem = createBrowserGroupBox();
    QGroupBox *descriptionMem = createDescriptionGroupBox();
    QGroupBox *details = createDetailsGroupBox();
    QGroupBox *bitsMem = createBitsGroupBox();
    QGroupBox *statusMem = createStatusGroupBox();
    QGroupBox *ctrlBits = createControlGroupBox();
    QGroupBox *statusBits = createStsBitsGroupBox();

    layoutMain = new QGridLayout;
    layoutMain->addWidget(positionMem, 0, 0);
    layoutMain->addWidget(descriptionMem, 1, 0, 3, 1);
    layoutMain->addWidget(statusMem, 0, 1, 1, 3);
    layoutMain->addWidget(details, 1, 1, 1, 3);
    layoutMain->addWidget(bitsMem, 2, 1, 1, 3);
    layoutMain->addWidget(ctrlBits, 3, 1, 1, 1);
    layoutMain->addWidget(statusBits, 3, 2, 1, 2);

    layoutMain->setColumnStretch(0, 1);
    layoutMain->setColumnStretch(1, 1);
    layoutMain->setColumnStretch(2, 2);
    layoutMain->setColumnStretch(3, 1);

    layoutMain->setRowStretch(0, 1);
    layoutMain->setRowStretch(1, 1);
    layoutMain->setRowStretch(2, 6);
    layoutMain->setRowStretch(3, 1);
    layoutMain->setColumnMinimumWidth(0, 500);

    hideLabels();

    QWidget *widget = new QWidget;
    widget->setLayout(layoutMain);
    setCentralWidget(widget);
}

void MainWindow::filter(const QString &filterText)
{
    groupButtonPressed = false;
    groupButtonSlot();

    for(int i = 0; i < memoryTable->rowCount(); ++i){
        memoryTable->setRowHidden(i, true);
    }

    QString auxText = "";

    for(int i = 1; i < memoryTable->rowCount(); ++i){
        for(int j = 0; j < memoryTable->columnCount(); ++j){
            QTableWidgetItem *item = memoryTable->item(i, j);
            if(item->text().contains(filterText, Qt::CaseInsensitive)){
                if(auxText != memoryTable->item(i, 3)->text()){
                    auxText = memoryTable->item(i, 3)->text();

                    QString size = memoryTable->item(i, 2)->text();
                    memoryTable->setRowHidden(i, false);

                    QString endPos = addedAddresses(memoryTable->item(i, 0)->text(), size);
                    memoryTable->setItem(i, 1, new QTableWidgetItem(endPos));
                }
                else{
                    memoryTable->setRowHidden(i, true);
                }
                break;
            }
        }
    }    
}

QString MainWindow::addedAddresses(QString initialPos, QString size)
{
    bool ok;
    int initialHex = initialPos.toInt(&ok, 16);
    int sizeHex = size.toInt(&ok, 16) - 1;
    initialHex += sizeHex;

    return("0x" + QString::number(initialHex, 16).toUpper().rightJustified(4, '0'));
}

void MainWindow::updateProgressBar()
{
    totalNoFree = 0;

    QMapIterator<int, QJsonObject> it(memoryMap);
    while (it.hasNext()) {
        it.next();
        QJsonObject obj = it.value();
        if(obj["Grupo"] != QJsonValue("FREE")){
            totalNoFree++;
        }
    }
    progressBar->setValue((float(totalNoFree) / float(memoryTable->rowCount())) * 100);
}

void MainWindow::configMemory()
{
    Config *config = new Config(this);
    int accepted = config->exec();

    if (accepted == 1) {
        QString value = "0x0000", size = "4";
        for (int i = 0; i < config->tamPos.toInt(); i++) {
            QJsonObject obj;
            if(i == 0){
                obj["lastModified"] = QJsonValue("Thu Jun 27 00:00:00 1996");
            }
            else{
                obj["Descripcion"] = QJsonValue(config->dataPos);
                obj["Desde"] = QJsonValue(value);
                obj["Grupo"] = QJsonValue("FREE");
                value = addedAddresses(value, size);
                obj["Hasta"] = QJsonValue(value);
                obj["Obs"] = QJsonValue("");
                obj["Peso"] = QJsonValue(size);

                QJsonArray array;
                for(int i = 0; i < 32; i++){
                    array.append(QJsonValue("0"));
                }
                obj["tableBits"] = array;
                value = addedAddresses(value, "2");
            }
            memoryMap[i] = obj;            
        }
    }
}

void MainWindow::lastModifiedFile(QString date)
{
    QJsonObject object;
    object["lastModified"] = QJsonValue(date);
    memoryMap[0] = object;
}

void MainWindow::showBitsDetails(bool show)
{
    editButton->setEnabled(show);
    fullButton->setEnabled(show);
    emptyButton->setEnabled(show);

    if(show){
        relationLabel->setText("Equivalencias:\n"
                               "1 posición = 1 word     ||     "
                               "1 word = 4 bytes     ||     "
                               "1 byte = 8 bits");
        relationLabel->show();
        editButton->show();
        fullButton->show();
        emptyButton->show();
        revertButton->show();
    }
    else{
        relationLabel->hide();
        beforeData->hide();
        revertButton->hide();
        editButton->hide();
        fullButton->hide();
        emptyButton->hide();
        revertButton->hide();
    }
}

void MainWindow::onTableWidgetCellClicked(int row, int column)
{
    bitSelectedRow = row;
    bitSelectedColumn = column;
    prevValueBit = bitsTable->item(row, column)->text();
    beforeData->setText("Valor actual: " + prevValueBit);
    beforeData->show();
    revertButton->setEnabled(false);
}

void MainWindow::editButtonSlot()
{
    QDialog dialog(this);
    dialog.setWindowTitle("Editar valor de bit");
    dialog.setModal(true);

    QLineEdit *lineEdit = new QLineEdit(&dialog);
    lineEdit->setGeometry(QRect(10, 10, 155, 20));

    QPushButton *acceptButton = new QPushButton(&dialog);
    acceptButton->setGeometry(QRect(10, 40, 75, 23));
    acceptButton->setIcon(QIcon(":/images/doneIcon.png"));

    QPushButton *cancelButton = new QPushButton(&dialog);
    cancelButton->setGeometry(QRect(90, 40, 75, 23));
    cancelButton->setIcon(QIcon(":/images/closeIcon.png"));

    connect(acceptButton, &QPushButton::clicked, &dialog, &QDialog::accept);
    connect(cancelButton, &QPushButton::clicked, &dialog, &QDialog::reject);

    if (dialog.exec() == QDialog::Accepted) {
        QString text = lineEdit->text();

        bitsTable->setItem(bitSelectedRow, bitSelectedColumn, new QTableWidgetItem(text));
        bitsTable->resizeColumnsToContents();
        beforeData->setText("Valor anterior: " + prevValueBit +
                            " -> Valor actual: " +  text);
        revertButton->setEnabled(true);
        revertButton->show();
        int idx = indexAdds.value(itemMain);
        idx += (bitSelectedRow + 1) / 4;
        idx--;
        if(idx == 0) idx = 1;

        QJsonArray tableBitsArray = memoryMap[idx]["tableBits"].toArray();
        tableBitsArray.replace(bitSelectedColumn, text);                
        memoryMap[idx]["tableBits"] = tableBitsArray;
    }
}

void MainWindow::fullButtonSlot()
{
    bitsTable->setItem(bitSelectedRow, bitSelectedColumn, new QTableWidgetItem("1"));
    bitsTable->resizeColumnsToContents();
    beforeData->setText("Valor anterior: " + prevValueBit +
                        " -> Valor actual: 1");
    revertButton->setEnabled(true);
    revertButton->show();
    int idx = indexAdds.value(itemMain);
    idx += (bitSelectedRow + 1) / 4;
    idx--;
    if(idx == 0) idx = 1;

    QJsonArray tableBitsArray = memoryMap[idx]["tableBits"].toArray();
    tableBitsArray.replace(bitSelectedColumn, "1");
    memoryMap[idx]["tableBits"] = tableBitsArray;
}

void MainWindow::emptyButtonSlot()
{
    bitsTable->setItem(bitSelectedRow, bitSelectedColumn, new QTableWidgetItem("0"));
    bitsTable->resizeColumnsToContents();
    beforeData->setText("Valor anterior: " + prevValueBit +
                        " -> Valor actual: 0");
    revertButton->setEnabled(true);
    revertButton->show();
    int idx = indexAdds.value(itemMain);
    idx += (bitSelectedRow + 1) / 4;
    idx--;
    if(idx == 0) idx = 1;

    QJsonArray tableBitsArray = memoryMap[idx]["tableBits"].toArray();
    tableBitsArray.replace(bitSelectedColumn, "0");
    memoryMap[idx]["tableBits"] = tableBitsArray;
}

void MainWindow::revertButtonSlot()
{
    bitsTable->setItem(bitSelectedRow, bitSelectedColumn, new QTableWidgetItem(prevValueBit));
    bitsTable->resizeColumnsToContents();
    beforeData->setText("Valor actual: " + prevValueBit);
    revertButton->setEnabled(false);
    int idx = indexAdds.value(itemMain);
    idx += (bitSelectedRow + 1) / 4;
    idx--;
    if(idx == 0) idx = 1;

    QJsonArray tableBitsArray = memoryMap[idx]["tableBits"].toArray();
    tableBitsArray.replace(bitSelectedColumn, prevValueBit);
    memoryMap[idx]["tableBits"] = tableBitsArray;
}

void MainWindow::groupButtonSlot()
{
    if(!groupButtonPressed){
        QString auxText = "";
        int sizeGroup = 4, line = 1;

        for(int i = 1; i < memoryTable->rowCount(); ++i){
            QTableWidgetItem *item = memoryTable->item(i, 3);
            if(auxText != item->text()){
                auxText = item->text();
                sizeGroup = 4;
                line = i;
            }
            else{
                memoryTable->setRowHidden(i, true);
                sizeGroup += 4;
                memoryTable->setItem(line, 1, new QTableWidgetItem(memoryMap[i].value("Hasta").toString()));
                memoryTable->setItem(line, 2, new QTableWidgetItem(QString::number(sizeGroup, 16).toUpper()));
            }
        }
        groupButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        groupButton->setIcon(QIcon(":/images/groupFolder2.png"));
        groupButton->setIconSize(QSize(30, 30));
        groupButtonPressed = true;
    }
    else{
        for(int i = 1; i < memoryTable->rowCount(); ++i){
            memoryTable->setRowHidden(i, false);
            memoryTable->setItem(i, 1, new QTableWidgetItem(memoryMap[i].value("Hasta").toString()));
            memoryTable->setItem(i, 2, new QTableWidgetItem("4"));
        }
        groupButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        groupButton->setIcon(QIcon(":/images/groupFolder.png"));
        groupButton->setIconSize(QSize(30, 30));
        groupButtonPressed = false;
    }
}

void MainWindow::filterButtonSlot()
{
    if(!filterButtonPressed){
        for(int i = 1; i < memoryTable->rowCount(); ++i){
            QTableWidgetItem *item = memoryTable->item(i, 3);
            if(item->text() != "FREE"){
                memoryTable->setRowHidden(i, false);
            }
            else{
                memoryTable->setRowHidden(i, true);
            }
        }
        filterButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        filterButton->setIcon(QIcon(":/images/filterIcon.png"));
        filterButton->setIconSize(QSize(30, 30));
        filterButtonPressed = true;
    }
    else{
        for(int i = 1; i < memoryTable->rowCount(); ++i){
            memoryTable->setRowHidden(i, false);
        }
        filterButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        filterButton->setIcon(QIcon(":/images/filterIcon2.png"));
        filterButton->setIconSize(QSize(30, 30));
        filterButtonPressed = false;
    }
    groupButtonPressed = false;
    groupButtonSlot();
}

void MainWindow::showPositDetails()
{
    QModelIndexList selection = memoryTable->selectionModel()->selectedRows();
    QJsonValue desdeShow, descripShow, pesoShow, groupShow, hastaShow, obsShow;

    if (!selection.empty()) {
        QModelIndex idIndex = selection.at(0);
        itemMain = idIndex.sibling(idIndex.row(), 0).data().toString();
        showBitsDetails(false);
        bitsTable->show();

        int idx = indexAdds.value(itemMain);

        descripShow = memoryMap[idx].value("Descripcion");

        groupShow = memoryMap[idx].value("Grupo");
        hastaShow = memoryMap[idx].value("Hasta");
        obsShow = memoryMap[idx].value("Obs");

        descripLabel->setText(tr("Descripción: %1\n"
                                 "Grupo: %2").arg(descripShow.toString()).arg(groupShow.toString()));
        descripLabel->show();

        if(groupButtonPressed){
            desdeShow = memoryMap[idx].value("Desde");
            pesoShow = memoryMap[idx].value("Peso");
        }
        else{
            desdeShow = itemMain;
            pesoShow = "4";
        }
        QString endAddress = addedAddresses(desdeShow.toString(), pesoShow.toString());
        QString sizeStr = pesoShow.toString();
        bool ok;
        int sizeDec = sizeStr.toInt(&ok, 16);
        int byte = endAddress.toInt(&ok, 16);

        infoLabel->setText(tr("Desde: %1\n"
                              "Hasta: %2\n"
                              "Peso[hex]: %3\n"
                              "Peso[dec]: %4").arg(desdeShow.toString()).arg(endAddress).arg(sizeStr).arg(sizeDec));
        infoLabel->show();

        obsLabel->setText(obsShow.toString());
        obsLabel->show();

        int rowNumbInt = sizeDec;
        bitsTable->setRowCount(rowNumbInt);

        for(int j = 0; j < rowNumbInt; j = j + 4){
            int bit = 31;
            for(int i = 0; i < 32; i++){
                bitsTable->setItem(j+3, i, new QTableWidgetItem(memoryMap[idx].value("tableBits")[i].toString()));

                QTableWidgetItem *item = new QTableWidgetItem("Bit " + QString::number(bit));
                bitsTable->setItem(j+2, i, item);

                if(!(i%8)){
                    bitsTable->setSpan(j+1, i, 1, 8);
                    QString byteStr = QString::number(byte, 16).toUpper().rightJustified(4, '0');
                    QTableWidgetItem *item2 = new QTableWidgetItem("Byte: 0x" + byteStr);
                    item2->setForeground(QColor(0, 0, 0));
                    bitsTable->setItem(j+1, i, item2);
                    byte--;
                }
                bit--;
            }
            bitsTable->setSpan(j, 0, 1, 32);
            QString initPos = addedAddresses(endAddress, "-2");
            QTableWidgetItem *word = new QTableWidgetItem("Word: " + endAddress + " - " + initPos);
            word->setBackground(QColor(255, 145, 0));
            word->setForeground(QColor(0, 0, 0));
            bitsTable->setItem(j, 0, word);
            endAddress = addedAddresses(initPos, "0");;
            idx++;
        }
        bitsTable->resizeColumnsToContents();
    }
}

void MainWindow::addPosit()
{
    Dialog *dialog = new Dialog(this);

    dialog->setMinimumSize(400, 100);
    int accepted = dialog->exec();

    if (accepted == 1) {       
        int idx = indexAdds.value(dialog->beginPos);
        if(memoryMap[idx]["Descripcion"] == "FREE"){
            bool ok;
            int countIdx = dialog->sizePos.toInt(&ok, 16) / 4;
            QString desdePos = dialog->beginPos;

            for(int j = 0; j < countIdx; j++){

                memoryMap[idx]["Desde"] = desdePos;
                memoryMap[idx]["Hasta"] = addedAddresses(desdePos, "4");
                memoryMap[idx]["Descripcion"] = dialog->descripPos;
                memoryMap[idx]["Grupo"] = dialog->groupPos;
                memoryMap[idx]["Obs"] = "Obs: " + dialog->obsPos;
                memoryMap[idx]["Peso"] = dialog->sizePos;

                QJsonArray tableBitsArray;
                for(int i = 0; i < 32; i++){
                    tableBitsArray.append("1");
                }
                memoryMap[idx]["tableBits"] = tableBitsArray;

                memoryTable->setItem(idx, 1, new QTableWidgetItem(memoryMap[idx].value("Hasta").toString()));
                memoryTable->setItem(idx, 2, new QTableWidgetItem(memoryMap[idx].value("Peso").toString()));
                memoryTable->setItem(idx, 3, new QTableWidgetItem(memoryMap[idx].value("Descripcion").toString()));
                memoryTable->setItem(idx, 4, new QTableWidgetItem(memoryMap[idx].value("Grupo").toString()));
                memoryTable->setRowHidden(idx, false);

                desdePos = addedAddresses(desdePos, "5");
                idx++;
            }
            showPositDetails();
        }
        else{
            QString message(tr("La posición a la que desea acceder ya se encuentra ocupada.\n"
                               "Para poder acceder a dicha posición deberá ser liberada con anterioridad."));
            QMessageBox::critical(this, tr("ERROR en la ubicación"), message);
        }
    }
    hideLabels();
    adjustHeader();
    updateProgressBar();
    groupButtonPressed = false;
    for(int k = 0; k < 3; k++) groupButtonSlot();
}

void MainWindow::deletePosit()
{
    QModelIndexList selection = memoryTable->selectionModel()->selectedRows();

    if (!selection.empty()) {
        QModelIndex idIndex = selection.at(0);
        QString desdePos = idIndex.sibling(idIndex.row(), 0).data().toString();
        QString descripPos = idIndex.sibling(idIndex.row(), 3).data().toString();
        QString sizePos = idIndex.sibling(idIndex.row(), 2).data().toString();

        QMessageBox::StandardButton button;
        button = QMessageBox::question(this, tr("Eliminar"), tr("Está seguro de que desea eliminar:\n"
                                                "Posición: %1\n"
                                                "Peso: %2\n"
                                                "Descripción: %3")
                        .arg(desdePos, sizePos, descripPos));

        if (button == QMessageBox::Yes) {
            bool ok;
            int idx = indexAdds.value(desdePos);
            int countIdx = sizePos.toInt(&ok, 16) / 4;

            for(int j = 0; j < countIdx; j++){
                memoryMap[idx]["Desde"] = desdePos;
                memoryMap[idx]["Hasta"] = addedAddresses(desdePos, "4");
                memoryMap[idx]["Descripcion"] = "FREE";
                memoryMap[idx]["Grupo"] = "FREE";
                memoryMap[idx]["Obs"] = "";
                memoryMap[idx]["Peso"] = "4";

                QJsonArray tableBitsArray;
                for(int i = 0; i < 32; i++){
                    tableBitsArray.append("0");
                }
                memoryMap[idx]["tableBits"] = tableBitsArray;

                memoryTable->setItem(idx, 0, new QTableWidgetItem(memoryMap[idx].value("Desde").toString()));
                memoryTable->setItem(idx, 1, new QTableWidgetItem(memoryMap[idx].value("Hasta").toString()));
                memoryTable->setItem(idx, 2, new QTableWidgetItem(memoryMap[idx].value("Peso").toString()));
                memoryTable->setItem(idx, 3, new QTableWidgetItem(memoryMap[idx].value("Descripcion").toString()));
                memoryTable->setItem(idx, 4, new QTableWidgetItem(memoryMap[idx].value("Grupo").toString()));
                memoryTable->setRowHidden(idx, false);

                desdePos = addedAddresses(desdePos, "5");
                idx++;
            }
        }
    }
    else {
        QMessageBox::information(this, tr("Eliminar"), tr("Selecciona la posición que deseas eliminar (liberar)."));
    }
    hideLabels();
    adjustHeader();
    updateProgressBar();
}

QGroupBox* MainWindow::createBrowserGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Buscador"));

    QLabel *iconLogoLabel = new QLabel;
    iconLogoLabel->setPixmap(QPixmap(":/images/logoLeistung.png").scaled(500, 70, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    QLineEdit *filterEdit = new QLineEdit;
    filterEdit->setPlaceholderText("Escribe aquí para buscar");
    filterEdit->setFixedSize(500, 30);

    QLabel *iconFindLabel = new QLabel;
    iconFindLabel->setPixmap(QPixmap(":/images/find.png").scaled(30, 30, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    connect(filterEdit, &QLineEdit::textChanged, this, &MainWindow::filter);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(iconLogoLabel, 0, 0);
    layout->addWidget(filterEdit, 1, 0);
    layout->addWidget(iconFindLabel, 1, 1);
    box->setLayout(layout);

    return box;
}

QGroupBox* MainWindow::createDescriptionGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Tabla de Memoria"));

    groupButton = new QPushButton;
    groupButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    groupButton->setIcon(QIcon(":/images/groupFolder.png"));
    groupButton->setIconSize(QSize(30, 30));
    QObject::connect(groupButton, &QPushButton::clicked,
                         this, &MainWindow::groupButtonSlot);
    groupButtonPressed = false;

    filterButton = new QPushButton;
    filterButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    filterButton->setIcon(QIcon(":/images/filterIcon2.png"));
    filterButton->setIconSize(QSize(30, 30));
    QObject::connect(filterButton, &QPushButton::clicked,
                         this, &MainWindow::filterButtonSlot);
    filterButtonPressed = false;

    addButton = new QPushButton;
    addButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    addButton->setIcon(QIcon(":/images/addIcon.png"));
    addButton->setIconSize(QSize(30, 30));
    QObject::connect(addButton, &QPushButton::clicked,
                         this, &MainWindow::addPosit);

    deleteButton = new QPushButton;
    deleteButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    deleteButton->setIcon(QIcon(":/images/deleteIcon.png"));
    deleteButton->setIconSize(QSize(30, 30));
    QObject::connect(deleteButton, &QPushButton::clicked,
                         this, &MainWindow::deletePosit);

    QHBoxLayout *layoutH = new QHBoxLayout;
    layoutH->addWidget(addButton, 0, {Qt::AlignRight});
    layoutH->addWidget(deleteButton, 0, {Qt::AlignLeft});
    layoutH->addWidget(groupButton, 0, {Qt::AlignRight});
    layoutH->addWidget(filterButton, 0, {Qt::AlignLeft});

    QStringList tableHeader;
    tableHeader<<"Desde\n[hex]"<<"Hasta\n[hex]"<<"Peso\n[hex]"<<"Descripción"<<"Grupo";
    memoryTable = new QTableWidget;
    memoryTable->setRowCount(memoryMap.count());
    memoryTable->setColumnCount(5);
    memoryTable->setHorizontalHeaderLabels(tableHeader);

    memoryTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    memoryTable->setSortingEnabled(true);
    memoryTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    memoryTable->setSelectionMode(QAbstractItemView::SingleSelection);
    memoryTable->setShowGrid(true);
    memoryTable->verticalHeader()->hide();
    memoryTable->setAlternatingRowColors(true);

    QString descripAux = "";
    QMapIterator<int, QJsonObject> it(memoryMap);
    while(it.hasNext()){
        it.next();
        int key = it.key();
        QJsonObject obj = it.value();

        if(key == 0){
            lastModified = QJsonValue(obj["lastModified"]).toString();
        }

        memoryTable->setItem(key, 0, new QTableWidgetItem(obj["Desde"].toString()));
        memoryTable->setItem(key, 1, new QTableWidgetItem(obj["Hasta"].toString()));
        memoryTable->setItem(key, 2, new QTableWidgetItem(obj["Peso"].toString()));
        memoryTable->setItem(key, 3, new QTableWidgetItem(obj["Descripcion"].toString()));
        memoryTable->setItem(key, 4, new QTableWidgetItem(obj["Grupo"].toString()));
        if(obj["Descripcion"] != QJsonValue("FREE")){
            totalNoFree++;
        }
        if(obj["Peso"] != QJsonValue("4")){
            if(descripAux != obj["Descripcion"].toString()){
                descripAux = obj["Descripcion"].toString();

                QString size = obj["Peso"].toString();
                memoryTable->setRowHidden(key, false);
                QString endPos = addedAddresses(obj["Desde"].toString(), size);
                memoryTable->setItem(key, 1, new QTableWidgetItem(endPos));
            }
            else{
                memoryTable->setRowHidden(key, true);
            }
        }
    }
    memoryTable->setRowHidden(0, true);
    adjustHeader();

    connect(memoryTable, &QTableView::clicked, this, &MainWindow::showPositDetails);
    connect(memoryTable, &QTableView::activated, this, &MainWindow::showPositDetails);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(layoutH);
    layout->addWidget(memoryTable, 0, {});
    box->setLayout(layout);

    return box;
}

QGroupBox* MainWindow::createDetailsGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Detalles"));

    descripLabel = new QLabel;
    descripLabel->setWordWrap(true);
    descripLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    infoLabel = new QLabel();
    infoLabel->setWordWrap(true);
    infoLabel->setAlignment(Qt::AlignTop | Qt::AlignRight);

    obsLabel = new QLabel;
    obsLabel->setWordWrap(true);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(descripLabel, 0, 0);
    layout->addWidget(infoLabel, 0, 2);
    layout->addWidget(obsLabel, 1, 0, 1, 3);
    box->setLayout(layout);
    return box;
}

QGroupBox* MainWindow::createBitsGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Tabla de Bits"));
    QStringList tableHeaderBits;
    bitsTable = new QTableWidget;

    bitsTable->setColumnCount(32);
    bitsTable->setHorizontalHeaderLabels(tableHeaderBits);

    bitsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    bitsTable->setSortingEnabled(false);
    bitsTable->setSelectionMode(QAbstractItemView::SingleSelection);
    bitsTable->setShowGrid(true);
    bitsTable->verticalHeader()->hide();
    bitsTable->horizontalHeader()->hide();
    bitsTable->setAlternatingRowColors(true); 
    bitsTable->hide();

    connect(bitsTable, &QTableView::clicked, this, [this](){
        showBitsDetails(true);
    });
    connect(bitsTable, &QTableView::activated, this, [this](){
        showBitsDetails(true);
    });
    connect(bitsTable, &QTableWidget::cellClicked, this, &MainWindow::onTableWidgetCellClicked);


    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(bitsTable, 0, {});
    box->setLayout(layout);

    return box;
}

QGroupBox *MainWindow::createStatusGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Estado Actual de Memoria"));

    QLabel *lastDate = new QLabel;
    lastDate->setText("Ultima modificación: " + lastModified);

    QLabel *totalMemory = new QLabel;
    totalMemory->setText(tr("Capacidad de almacenamiento: 32 KBytes = 256 Kbits"));

    QLabel *totalPositions = new QLabel;
    totalPositions->setText(tr("Total de posiciones: 8000 [hex] = 32768 [dec] (1 posición = 4 bytes)"));

    QLabel *fullPositions = new QLabel;
    fullPositions->setText(tr("Posiciones ocupadas: %1 [hex]").arg(totalNoFree));

    QLabel *emptyPositions = new QLabel;
    int totalFree = 8000 - totalNoFree;
    emptyPositions->setText(tr("Posiciones libres. %1 [hex]").arg(totalFree));

    progressBar = new QProgressBar;
    progressBar->setRange(0, 100);

    updateProgressBar();

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(lastDate, 0, 2);
    layout->addWidget(totalMemory, 0, 0);
    layout->addWidget(totalPositions, 1, 0);
    layout->addWidget(fullPositions, 2, 0);
    layout->addWidget(emptyPositions, 3, 0);
    layout->addWidget(progressBar, 4, 0, 1 ,3);
    box->setLayout(layout);
    return box;
}

QGroupBox *MainWindow::createStsBitsGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Status de Bits"));

    relationLabel = new QLabel;
    relationLabel->setWordWrap(true);
    relationLabel->setAlignment(Qt::AlignTop | Qt::AlignCenter);

    beforeData = new QLabel;

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(relationLabel, 0, 0, 1, 3);
    layout->addWidget(beforeData, 1, 0, 1, 2);

    box->setLayout(layout);

    return box;
}

QGroupBox *MainWindow::createControlGroupBox()
{
    QGroupBox *box = new QGroupBox(tr("Control de Bits"));

    editButton = new QPushButton;
    editButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    editButton->setIcon(QIcon(":/images/editIcon.png"));
    editButton->setIconSize(QSize(40, 40));
    QObject::connect(editButton, &QPushButton::clicked,
                         this, &MainWindow::editButtonSlot);

    fullButton = new QPushButton;
    fullButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    fullButton->setIcon(QIcon(":/images/oneIcon.png"));
    fullButton->setIconSize(QSize(40, 40));
    QObject::connect(fullButton, &QPushButton::clicked,
                     this, &MainWindow::fullButtonSlot);

    emptyButton = new QPushButton;
    emptyButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    emptyButton->setIcon(QIcon(":/images/zeroIcon.png"));
    emptyButton->setIconSize(QSize(40, 40));
    QObject::connect(emptyButton, &QPushButton::clicked,
                     this, &MainWindow::emptyButtonSlot);

    revertButton = new QPushButton;
    revertButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    revertButton->setIcon(QIcon(":/images/revertIcon.png"));
    revertButton->setIconSize(QSize(40, 40));
    QObject::connect(revertButton, &QPushButton::clicked,
                         this, &MainWindow::revertButtonSlot);

    editButton->setEnabled(false);    
    fullButton->setEnabled(false);
    emptyButton->setEnabled(false);
    revertButton->setEnabled(false);

    editButton->hide();
    fullButton->hide();
    emptyButton->hide();
    revertButton->hide();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(editButton, 0);
    layout->addWidget(fullButton, 0);
    layout->addWidget(emptyButton, 0);
    layout->addWidget(revertButton, 0);

    box->setLayout(layout);

    return box;
}

void MainWindow::createMenuBar()
{
    openAction = new QAction(tr("&Abrir"), this);
    addAction = new QAction(tr("&Agregar"), this);
    deleteAction = new QAction(tr("&Eliminar"), this);
    saveAction = new QAction(tr("&Guardar"), this);
    configAction = new QAction(tr("&Configuración"), this);
    quitAction = new QAction(tr("&Salir"), this);
    aboutAction = new QAction(tr("&Acerca de"), this);

    openAction->setShortcut(tr("Ctrl+O"));
    addAction->setShortcut(tr("Ctrl+A"));
    saveAction->setShortcut(tr("Ctrl+S"));
    deleteAction->setShortcut(tr("Ctrl+D"));
    quitAction->setShortcuts(QKeySequence::Quit);

    QMenu *fileMenu = menuBar()->addMenu(tr("&Archivo"));
    fileMenu->addAction(openAction);
    fileMenu->addAction(addAction);
    fileMenu->addAction(deleteAction);
    fileMenu->addSeparator();
    fileMenu->addAction(configAction);
    fileMenu->addSeparator();
    fileMenu->addAction(saveAction);
    fileMenu->addAction(quitAction);

    QMenu *helpMenu = menuBar()->addMenu(tr("&Ayuda"));
    helpMenu->addAction(aboutAction);

    connect(openAction, &QAction::triggered, this, &MainWindow::openFile);
    connect(addAction, &QAction::triggered, this, &MainWindow::addPosit);
    connect(deleteAction, &QAction::triggered, this, &MainWindow::deletePosit);
    connect(saveAction, &QAction::triggered, this, &MainWindow::saveFile);
    connect(configAction, &QAction::triggered, this, &MainWindow::configMemory);
    connect(quitAction, &QAction::triggered, this, &MainWindow::close);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::about);

    addAction->setEnabled(false);
    deleteAction->setEnabled(false);
    saveAction->setEnabled(false);
}

void MainWindow::hideLabels()
{
    infoLabel->hide();
    descripLabel->hide();
    obsLabel->hide();
    bitsTable->hide();
}

void MainWindow::openFile()
{
    QString json_filter = "JSON (*.json)";
    QString nameFile = QFileDialog::getOpenFileName(this, tr("Abrir archivo"), "/",
                                                    json_filter, &json_filter,
                                                    QFileDialog::DontUseNativeDialog);
    if(nameFile.isEmpty()){
        qDebug() << "esta vacio";
    }
    else{
        QJsonDocument doc;
        QByteArray data_json;
        QFile input(nameFile);

        if(input.open(QIODevice::ReadOnly | QIODevice::Text)){
            qDebug() << "Leyendo...";
            data_json = input.readAll();
            doc = doc.fromJson(data_json);
            QJsonArray array = doc.array();

            for (int i = 0; i < array.size(); i++) {
                QJsonObject obj = array[i].toObject();
                memoryMap[i] = obj;
            }

            addAction->setEnabled(true);
            deleteAction->setEnabled(true);
            saveAction->setEnabled(true);

            for (auto i = memoryMap.begin(); i != memoryMap.end(); i++) {
                QJsonObject actualValue = i.value();
                QString actualAdds = actualValue["Desde"].toString();
                indexAdds.insert(actualAdds, i.key());
            }
            initialProgram();
        }
        else{
            qDebug() << "Error read";
        }
    }
}

void MainWindow::saveFile()
{
    QString json_filter = "JSON (*.json)";
    QString nameFile = QFileDialog::getSaveFileName(this, tr("Guardar archivo"), "/",
                                                    json_filter, &json_filter,
                                                    QFileDialog::DontUseNativeDialog);
    if(nameFile.isEmpty()){

    }
    else{
        QFile outFile(nameFile);
        QFileInfo fileInfo(outFile);
        QJsonArray newArray;

        lastModifiedFile(fileInfo.lastModified().toString());

        QMapIterator<int, QJsonObject> it(memoryMap);
        while (it.hasNext()) {
            it.next();
            QJsonObject obj = it.value();
            newArray.append(obj);
        }
        if(outFile.open(QIODevice::WriteOnly | QIODevice::Text)){
            QJsonDocument newDoc(newArray);
            QByteArray data_json = newDoc.toJson();
            outFile.write(data_json);
            outFile.close();
        }
        else{
            qDebug() << "Error";
        }
    }
}

void MainWindow::adjustHeader()
{
    memoryTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    memoryTable->resizeColumnToContents(0);
    memoryTable->resizeColumnToContents(1);
    memoryTable->resizeColumnToContents(2);
    memoryTable->resizeColumnToContents(3);
    memoryTable->resizeColumnToContents(4);
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("Mapeo de Memoria FRAM"),
            tr("<p>El <b>mapeo y gestión de memoria</b> se refiere a la asignación y control "
               "del espacio de memoria disponible para un programa en un sistema computacional. "
               "Estos procesos son críticos para garantizar que los programas tengan acceso al "
               "espacio de memoria necesario y que los recursos de memoria del sistema se "
               "utilicen de manera eficiente.</p>"

               "<p>El objetivo de este programa es administrar de manera eficiente y ordenada la "
               "memoria de comunicación entre la plataforma de control y la interfaz de usuario "
               "(GUI) del equipo LUFT5 de Leistung+. Además, permite la posibilidad de crear "
               "confirmaciones del uso de la memoria en un almacenamiento remoto, lo que garantiza "
               "un acceso actualizado y compartido de la información para todo el equipo de "
               "trabajo.</p>"

               "<p>Realizado en Qt 6.2.4 - Base de datos local en formato: JSON</p>"

               "<p>Ing. González, Alexis - Febrero 2023 - v0.1.0</p>"));
}

